﻿using Plugin.Toasts.UWP;
using Xamarin.Forms;

namespace R2.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            LoadApplication(new R2.App());

            DependencyService.Register<ToastNotification>(); // Register your dependency
            ToastNotification.Init();
        }
    }
}