﻿using R2.Models;

namespace R2.Config
{
    public class Config
    {
        protected static DriverModel _driverModel;
        public static string Api => "http://intern-1.internship.itembridge.com/?rest_route=/api/v1";

        public static DriverModel CurrentDriver => _driverModel ?? (_driverModel = new DriverModel());
    }
}
