﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace R2.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
		public AboutPage ()
		{
			InitializeComponent ();
		}
	}
}
