﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using R2.Components;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MenuItem = R2.Components.MenuItem;


namespace R2.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        protected NavigationPage NavigationPage;
        protected Dictionary<string, Page> Pages;

        public MainPage()
        {
            InitializeComponent();

            InitPages();

            NavigationPage = new NavigationPage(Pages["login"])
            {
                BarBackgroundColor = Color.FromHex("#009688"),
                BarTextColor = Color.White
            };
            Detail = NavigationPage;

            InitMenu(false, null);
            LstMainNav.ItemSelected += OnMenuItemSelected;
            LoginManager.GetInstance().OnLoginStateChange += InitMenu;
        }

        protected void InitPages()
        {
            Pages = new Dictionary<string, Page>
            {
                {"login", new LoginPage()},
                {"location", new LocationPage()},
                {"about", new AboutPage()}
            };
        }


        protected void InitMenu(bool secure, JObject response)
        {
            List<MenuItem> menuItems;
            if (secure)
            {
                menuItems = new List<MenuItem>
                {
                    new MenuItem("Account", Pages["login"]),
                    new MenuItem("Location info", Pages["location"]),
                    new MenuItem("About", Pages["about"])
                };
            }
            else
            {
                menuItems = new List<MenuItem>
                {
                    new MenuItem("Login", Pages["login"]),
                    new MenuItem("About", Pages["about"])
                };
            }

            LstMainNav.BindingContext = menuItems;
            LstMainNav.ItemsSource = menuItems;
        }

        public async void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            IsPresented = false;
            var selected = args.SelectedItem as MenuItem;
            LstMainNav.SelectedItem = null;

            if (!CheckAvailableTransaction(selected)) return;

            await NavigationPage.PushAsync(selected?.LinkedPage, true);
            NavigationPage.Title = selected?.LinkedPage?.Title;


        }

        protected bool CheckAvailableTransaction(MenuItem item)
        {
            return item?.LinkedPage != null && item.LinkedPage != NavigationPage.CurrentPage;
        }
    }
}