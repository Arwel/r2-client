﻿using System;
using Newtonsoft.Json.Linq;
using R2.Components;
using R2.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace R2.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

            ShowForm();
            LoginManager.GetInstance().OnLoginStateChange += InitForms;
            BtnLogIn.Clicked += LogInClick;
            BtnLogOut.Clicked += LogOutClick;
            BtnChooseRoute.Clicked += BtnChooseRouteClick;
            PckrRoute.SelectedIndexChanged += PckrRouteOnSelectedIndexChanged;
            BtnLoadSound.Clicked += BtnLoadSoundOnClicked;

            var resourceService = ResourceService.Current;
            resourceService.OnDownloadStatusChange += OnDownloadProgress;
            resourceService.OnDownloadComplete += OnDownloadComplete;
        }

        public void OnDownloadProgress(int current, int total, string name)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StatuFrame.IsVisible = true;
                StatuFrame.FadeTo(1, 250, Easing.CubicIn);

                LblDownloadStatus.Text = $"( {current} / {total} ) - {name}";
            });
        }

        public void OnDownloadComplete()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StatuFrame.FadeTo(0, 250, Easing.CubicIn);
                StatuFrame.IsVisible = false;

                LblDownloadStatus.Text = "";
            });

            UnlockButtons();
        }

        protected void LockButtons()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                BtnChooseRoute.FadeTo(0.5, 250, Easing.CubicIn);
                BtnLoadSound.FadeTo(0.5, 250, Easing.CubicIn);

                BtnChooseRoute.IsEnabled = false;
                BtnLoadSound.IsEnabled = false;
            });
        }

        protected void UnlockButtons()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                BtnChooseRoute.IsEnabled = true;
                BtnLoadSound.IsEnabled = true;

                BtnChooseRoute.FadeTo(1, 250, Easing.CubicIn);
                BtnLoadSound.FadeTo(1, 250, Easing.CubicIn);
            });
        }

        private void BtnLoadSoundOnClicked(object sender, EventArgs eventArgs)
        {
            var routeSelectedIndex = PckrRoute.SelectedIndex;
            var routes = Config.Config.CurrentDriver.RoutesList;


            if (routeSelectedIndex > -1 && routes.Count > routeSelectedIndex)
            {
                LockButtons();

                var routeId = routes[routeSelectedIndex].Id;
                LoginManager.GetInstance().LoadSoundsRequest(routeId);
            }
        }

        private void PckrRouteOnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            if (PckrRoute.SelectedIndex < 0)
            {
                return;
            }
            ShowButtons();
        }

        public void InitForms(bool sequre, JObject response)
        {
            if (sequre)
            {
                ShowAccount();
                HideButtons();
            }
            else
            {
                HideError();
                var status = response?.SelectToken("status").Value<string>();
                var error = "";
                if (status != "OK")
                {
                    error = response?.SelectToken("message").Value<string>();
                }

                if (!string.IsNullOrEmpty(error))
                {
                    ShowError(error);
                }

                ShowForm();
            }
        }

        public async void BtnChooseRouteClick(object sender, EventArgs e)
        {
            var routeSelectedIndex = PckrRoute.SelectedIndex;
            var routes = Config.Config.CurrentDriver.RoutesList;
            if (routeSelectedIndex > -1 && routes.Count > routeSelectedIndex)
            {
                LockButtons();

                var routeId = routes[routeSelectedIndex].Id;
                var loginManager = LoginManager.GetInstance();

                var result = await loginManager.ChooseRouteRequest(routeId);

                if (result != "OK")
                {
                    await DisplayAlert("Error", result, "OK");
                }

                var stopsResult = await loginManager.GetStopsRequest(routeId);

                if (stopsResult != "OK")
                {
                    await DisplayAlert("Error", stopsResult, "OK");
                }

                UnlockButtons();
                HideButtons();
            }
        }

        public async void LogInClick(object sender, EventArgs e)
        {
            HideError();

            var loginManager = LoginManager.GetInstance();
            loginManager.Login = TxtLogIn.Text?.Trim();
            loginManager.Password = TxtPassword.Text?.Trim();

            if (string.IsNullOrEmpty(loginManager.Login) || string.IsNullOrEmpty(loginManager.Password))
            {
                ShowError("Login and passwors can`t be empty!");
                return;
            }

            await loginManager.LoginRequest();
            FillRouteSelect();
        }

        public void FillRouteSelect()
        {
            PckrRoute.Items.Clear();
            foreach (var routeModel in Config.Config.CurrentDriver.RoutesList)
            {
                PckrRoute.Items.Add(routeModel.Name);
            }
        }

        public async void LogOutClick(object sender, EventArgs e)
        {
            await LoginManager.GetInstance().LogoutRequest();
            HideButtons();
        }

        protected void ShowButtons()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RouteControlBlock.IsVisible = true;
                RouteControlBlock.FadeTo(1, 250, Easing.CubicIn);
            });
        }

        protected void HideButtons()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RouteControlBlock.FadeTo(0, 250, Easing.CubicIn);
                RouteControlBlock.IsVisible = false;
            });
        }

        protected void ShowForm()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                LogInForm.IsVisible = true;
                LogInForm.FadeTo(1, 250, Easing.CubicIn);
                LogOutForm.FadeTo(0, 250, Easing.CubicIn);
                LogOutForm.IsVisible = false;
            });
        }

        protected void ShowAccount()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var name = Config.Config.CurrentDriver.Name;

                LblName.Text =
                    $"Hi {name}, you successfully log in to driver account. " +
                    "Now you must choose route for start share your location. ";

                LogInForm.FadeTo(0, 250, Easing.CubicIn);
                LogInForm.IsVisible = false;
                LogOutForm.IsVisible = true;
                LogOutForm.FadeTo(1, 250, Easing.CubicIn);
            });
        }

        protected void ShowError(string msg)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                LblLoginError.Text = msg;
                LblLoginError.IsVisible = true;
                LblLoginError.FadeTo(1, 250, Easing.CubicIn);
            });
        }

        protected void HideError()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                LblLoginError.Text = "";
                LblLoginError.FadeTo(0, 250, Easing.CubicIn);
                LblLoginError.IsVisible = false;
            });
        }
    }
}