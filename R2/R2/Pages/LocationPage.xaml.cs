﻿using System;
using R2.Models;
using R2.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace R2.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationPage : ContentPage
    {
        protected DateTime timeStamp;
        protected LocationModel lastLocationModel;

        public LocationPage()
        {
            InitializeComponent();

            timeStamp = DateTime.Now;
            LocationService.GetInstance().OnLocationUpdate += LocationChange;
        }

        public async void LocationChange(LocationModel location)
        {
            var timeDiff = DateTime.Now - timeStamp;
            if (timeDiff < TimeSpan.FromSeconds(2) || lastLocationModel == location)
            {
                return;
            }

            lastLocationModel = location;

            var locationService = LocationService.GetInstance();
            var nearestPoint = locationService.GetNearestPoint(location);
            if (nearestPoint != locationService.currentStopModel && !string.IsNullOrEmpty(nearestPoint.SoundUrl))
            {
                locationService.currentStopModel = nearestPoint;
                var resourceService = ResourceService.Current;
                var fileName = resourceService.ParseFileName(nearestPoint.SoundUrl);
                ResourceService.Current.Play(fileName);
            }
            await locationService.UpdateLocation(location);

            Device.BeginInvokeOnMainThread(() =>
            {
                LblNextStop.Text = nearestPoint?.Title;
                LblLatitude.Text = location.Latitude.ToString();
                LblLongitude.Text = location.Longitude.ToString();
                LblSpeed.Text = location.Speed.ToString();
            });
        }
    }
}