﻿namespace R2.Models
{
    public class LocationModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Speed { get; set; }

        public LocationModel()
        {
            Latitude = 0;
            Longitude = 0;
            Speed = 0;
        }

        public override string ToString()
        {
            return $"{Latitude} : {Longitude} ({Speed})";
        }
    }
}