﻿namespace R2.Models
{
    public class RouteModel
    {
        public RouteModel()
        {
        }

        public RouteModel(object source)
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Time { get; set; }
        public bool IsPlay { get; set; }

        public override string ToString()
        {
            return $"{Id}: {Name} - {Time} - {IsPlay}";
        }
    }
}