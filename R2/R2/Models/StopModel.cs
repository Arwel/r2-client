﻿using System;

namespace R2.Models
{
    public class StopModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Time { get; set; }
        public Decimal Lat { get; set; }
        public Decimal Lng { get; set; }
        public bool Play { get; set; }
        public double Barier { get; set; }
        public string SoundUrl { get; set; }

        public StopModel()
        {
            Id = 0;
            Time = "";
            Title = "";
            Lat = 0.0M;
            Lng = 0.0M;
            Play = false;
            Barier = 0.001;
            SoundUrl = "";
        }

        public override string ToString()
        {
            return $"{Id} {Time} {Title} {Lat} {Lng} {Play}";
        }

        public double GetDistance(LocationModel location)
        {
            var x = Convert.ToDouble(Lat) - location.Latitude;
            var y = Convert.ToDouble(Lng) - location.Longitude;

            return Math.Sqrt(Math.Pow(x, 2)) + Math.Sqrt(Math.Pow(y, 2));
        }

        public bool IsInBarier(LocationModel location)
        {
            return GetDistance(location) <= Barier;
        }
    }
}