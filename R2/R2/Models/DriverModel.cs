﻿using System.Collections.Generic;

namespace R2.Models
{
    public class DriverModel
    {
        public DriverModel()
        {
            Clear();
        }

        public string Name { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public string Error { get; set; }
        public int Id { get; set; }
        public List<RouteModel> RoutesList { get; set; }
        public List<StopModel> StopList { get; set; }

        public void Clear()
        {
            Name = "";
            Password = "";
            Login = "";
            Error = "ERROR";
            RoutesList = new List<RouteModel>();
            StopList = new List<StopModel>();
        }
    }
}