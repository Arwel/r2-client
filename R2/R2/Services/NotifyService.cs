﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace R2.Services
{
    public class NotifyService
    {
        protected Page page;

        protected static NotifyService instance;

        protected NotifyService()
        {
            page = new Page();
        }

        public static NotifyService GetInstance()
        {
            return instance ?? (instance = new NotifyService());
        }

        public async Task Notify(string text)
        {
            await page.DisplayAlert("Info", text, "OK");
        }

        public async Task Notify(string title, string text)
        {
            await page.DisplayAlert(title, text, "OK");
        }
    }
}