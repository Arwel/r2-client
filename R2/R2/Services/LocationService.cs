﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ModernHttpClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using R2.Components;
using R2.Models;
using Xamarin.Forms;


namespace R2.Services
{
    public class LocationService
    {
        protected object lockObject;
        protected object lockObject2;
        protected static LocationService instance;
        protected IGeolocator locator;
        protected bool start;
        public StopModel currentStopModel;
        protected LocationModel lastLocation;
        protected bool msgShowed;
        protected bool updateLocationLoker;
        protected Position lastPosition;

        public delegate void locationUpdateDelegate(LocationModel location);

        public locationUpdateDelegate OnLocationUpdate;

        protected async Task<bool> IsUpdateLocationAvailable(LocationModel location)
        {
            var loginManager = LoginManager.GetInstance();
            return !await loginManager.IsNetworkAvailable() ||
                   location == lastLocation ||
                   loginManager.IsLoggedIn ||
                   updateLocationLoker;
        }

        protected LocationService()
        {
            lockObject = "lock-object";
            lockObject2 = "lock-object-2";
            start = false;
            locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 20;
            locator.AllowsBackgroundUpdates = true;
            locator.PositionChanged += LocatorOnPositionChanged;
            currentStopModel = null;
            lastLocation = new LocationModel();
            lastLocation = null;

        }

        protected void LocatorOnPositionChanged(object sender, PositionEventArgs positionEventArgs)
        {
            if (!LoginManager.GetInstance().IsLoggedIn)
            {
                return;
            }

            var position = positionEventArgs.Position;
            if (position == lastPosition)
            {
                return;
            }

            OnLocationUpdate(new LocationModel()
            {
                Longitude = position.Longitude,
                Latitude = position.Latitude,
                Speed = position.Speed
            });
        }

        public async void Start()
        {
            if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
            {
                await NotifyService.GetInstance().Notify("Error", "GPS service is unavailable!");
                return;
            }

            await locator.StartListeningAsync(2000, 15);
        }

        public async void Stop()
        {
            if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
            {
                await NotifyService.GetInstance().Notify("Error", "GPS service is unavailable!");
                return;
            }

            await locator.StopListeningAsync();
        }

        public static LocationService GetInstance()
        {
            return instance ?? (instance = new LocationService());
        }

        public StopModel GetNearestPoint(LocationModel location)
        {
            var maxDistance = 9999.0;
            StopModel nearestPoint = null;
            foreach (var stopModel in Config.Config.CurrentDriver.StopList)
            {
                var distance = stopModel.GetDistance(location);
                if (distance < maxDistance)
                {
                    nearestPoint = stopModel;
                    maxDistance = distance;
                }
            }

            return nearestPoint;
        }

        public async Task<HttpResponseMessage> UpdateLocation(LocationModel location)
        {
            var loginManager = LoginManager.GetInstance();
            if (!await IsUpdateLocationAvailable(location))
            {
                return null;
            }

            updateLocationLoker = true;
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth", loginManager.CredentialsToken);
                httpClient.DefaultRequestHeaders.Add("X-Token", loginManager.Token);

                var httpContent = new StringContent(JsonConvert.SerializeObject(new
                {
                    lat = location.Latitude,
                    lng = location.Longitude,
                    speed = location.Speed
                }), Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync(Config.Config.Api + "/update", httpContent);
                var response = await result.Content.ReadAsStringAsync();

                var decodedResponse = JsonConvert.DeserializeObject(response) as JObject;
                var status = decodedResponse?.SelectToken("status").Value<string>();

                if (status != "OK")
                    await NotifyService.GetInstance().Notify("Error", "Location update falied!");

                updateLocationLoker = false;
                return result;
            }
        }
    }
}