﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using PCLStorage;
using Plugin.SimpleAudioPlayer.Abstractions;
using FileAccess = PCLStorage.FileAccess;

namespace R2.Services
{
    class ResourceService
    {
        protected static ResourceService _instance;
        protected object lockKey;

        public delegate void downloadStatusChagneDelegate(int currentFile, int totalFiles, string name);

        public downloadStatusChagneDelegate OnDownloadStatusChange;

        public delegate void downloadCompleteDelegate();

        public downloadCompleteDelegate OnDownloadComplete;
        public List<string> Queue { get; }

        public static ResourceService Current
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ResourceService();
                }
                ;
                return _instance;
            }
        }

        public ResourceService()
        {
            Queue = new List<string>();
            lockKey = "VoiceLockKey";
        }

        public string ParseFileName(string url)
        {
            var lastSlashIndex = url.LastIndexOf("/", StringComparison.Ordinal);
            if (lastSlashIndex + 1 >= url.Length)
            {
                return "";
            }

            return url.Substring(lastSlashIndex + 1);
        }

        public async void Play(string fileName)
        {
            var folder = FileSystem.Current.LocalStorage;
            folder = await folder.CreateFolderAsync("r2_sounds", CreationCollisionOption.OpenIfExists);
            if (await folder.CheckExistsAsync(fileName) != ExistenceCheckResult.FileExists)
            {
                Console.WriteLine($"File not found! --- {fileName}");
                return;
            }

            var soundFile = await folder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);
            try
            {
                ISimpleAudioPlayer player = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
                player.Load(await soundFile.OpenAsync(FileAccess.Read));
                player.Play();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void DownloadStart()
        {
            var folder = FileSystem.Current.LocalStorage;
            folder = await folder.CreateFolderAsync("r2_sounds", CreationCollisionOption.OpenIfExists);

            var index = 0;
            var total = Queue.Count;
            foreach (string url in Queue)
            {
                var name = ParseFileName(url);
                var file = await folder.CreateFileAsync(name, CreationCollisionOption.ReplaceExisting);
                OnDownloadStatusChange(++index, total, name);

                try
                {
                    using (var client = new HttpClient())
                    using (var response = await client.GetAsync(url))
                    using (var responseStream = await response.Content.ReadAsStreamAsync())
                    using (Stream writeStream = await file.OpenAsync(FileAccess.ReadAndWrite))
                    {
                        var contentLength = Convert.ToInt32(responseStream?.Length);
                        var buffer = new byte[contentLength];

                        var recived = await responseStream?.ReadAsync(buffer, 0, contentLength);
                        await writeStream.WriteAsync(buffer, 0, recived);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"+++++++++++++++++++++++++++++++++++++++++++++{e.Message}+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                }
            }
            OnDownloadComplete();

            Queue.Clear();
        }
    }
}