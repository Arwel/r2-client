﻿using Xamarin.Forms;

namespace R2.Components
{

    public class MenuItem
    {
        public MenuItem()
        {
        }

        public MenuItem(string title)
        {
            Title = title;
            this.LinkedPage = null;
        }

        public MenuItem(string title, Page linkedPage)
        {
            Title = title;
            LinkedPage = linkedPage;
        }

        public string Title { get; set; }

        public Page LinkedPage { get; set; }
    }
}
