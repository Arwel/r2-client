﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ModernHttpClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using R2.Models;
using R2.Services;
using static R2.Config.Config;


namespace R2.Components
{
    class LoginManager
    {
        protected static LoginManager instance;
        protected string _token;
        protected string _credentialsToken;
        public int LoadedRouteId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsLoggedIn => _loggedIn;
        protected bool _loggedIn = false;
        public string Token => _token;
        public string CredentialsToken => _credentialsToken;

        // lokers
        protected bool loginLocker;

        protected bool stopsLocker;
        protected bool routesLocker;
        protected bool chooseRoutesLocker;
        protected bool loadSoundsLocker;
        protected bool logoutLocker;

        public delegate void LoginStateChangeDelegate(bool isLoggedIn, JObject response);

        public LoginStateChangeDelegate OnLoginStateChange;

        protected LoginManager()
        {
            _token = "";
            _credentialsToken = "";
            LoadedRouteId = -1;
            ResourceService.Current.OnDownloadComplete += OnDownloadComplete;
        }

        public void OnDownloadComplete()
        {
            loadSoundsLocker = false;
        }

        public async Task<bool> IsNetworkAvailable()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;
            if (!isConnected)
            {
                await NotifyService.GetInstance().Notify("Error", "Network is unavailable");
            }

            return isConnected;
        }

        public static LoginManager GetInstance()
        {
            if (instance == null || instance.GetType() != typeof(LoginManager))
            {
                instance = new LoginManager();
            }

            return instance;
        }

        protected string Encode()
        {
            var jsonEncoded = JsonConvert.SerializeObject(new
            {
                user_login = Login,
                user_password = Password
            });

            var urlEncoded = WebUtility.UrlEncode(jsonEncoded);
            var utf8Bytes = Encoding.UTF8.GetBytes(urlEncoded);
            return Convert.ToBase64String(utf8Bytes);
        }

        public async Task<HttpResponseMessage> LoginRequest()
        {
            if (!await IsNetworkAvailable() || loginLocker || IsLoggedIn)
            {
                return null;
            }

            loginLocker = true;
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                var apiAddress = Api;
                _credentialsToken = Encode();

                httpClient.DefaultRequestHeaders.Add("X-Auth", _credentialsToken);
                var result = await httpClient.PostAsync(apiAddress + "/login", null);
                var responseRaw = await result.Content.ReadAsStringAsync();

                var decodedResponse = JsonConvert.DeserializeObject(responseRaw) as JObject;
                var status = decodedResponse?.SelectToken("status").Value<string>();

                if (status == "OK")
                {
                    var loggedIn = Convert.ToBoolean(decodedResponse?.SelectToken("data")?.SelectToken("login")
                        .Value<bool>());

                    _token = loggedIn
                        ? decodedResponse?.SelectToken("data")?.SelectToken("token").Value<string>()
                        : "";

                    CurrentDriver.Name = loggedIn
                        ? decodedResponse?.SelectToken("data")?.SelectToken("driver").Value<string>()
                        : "";
                    CurrentDriver.Id = loggedIn
                        ? Convert.ToInt32(decodedResponse?.SelectToken("data")?.SelectToken("driver_id")?.Value<int>())
                        : 0;

                    _loggedIn = true;

                    await AvailableRoutesRequest();

                    OnLoginStateChange(loggedIn, decodedResponse);
                    
                }
                else
                {
                    _token = "";
                    CurrentDriver.Name = "";
                    CurrentDriver.Id = 0;
                    _credentialsToken = "";
                    OnLoginStateChange(false, decodedResponse);
                    _loggedIn = false;
                }

                loginLocker = false;
                return result;
            }
        }

        protected async Task<HttpResponseMessage> AvailableRoutesRequest()
        {
            if (!await IsNetworkAvailable() || routesLocker || !IsLoggedIn)
            {
                return null;
            }

            routesLocker = true;
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth", _credentialsToken);
                httpClient.DefaultRequestHeaders.Add("X-Token", _token);

                var result = await httpClient.GetAsync(Api + "/routes/driver/" + CurrentDriver.Id);
                var response = await result.Content.ReadAsStringAsync();

                var decodedResponse = JObject.Parse(response);
                var status = decodedResponse?.SelectToken("status").Value<string>();

                if (status != "OK") return result;

                CurrentDriver.RoutesList.Clear();
                try
                {
                    var routesArr = decodedResponse.SelectToken("routes") as JArray;
                    if (routesArr != null)
                    {
                        foreach (var jToken1 in routesArr)
                        {
                            var jToken = (JObject) jToken1;
                            var routeModel = new RouteModel
                            {
                                Id = Convert.ToInt32(jToken.SelectToken("id")?.Value<int>()),
                                Name = jToken.SelectToken("title")?.Value<string>(),
                                Time = jToken.SelectToken("time")?.Value<string>(),
                                IsPlay = Convert.ToBoolean(jToken.SelectToken("play")?.Value<bool>())
                            };
                            CurrentDriver.RoutesList.Add(routeModel);
                        }
                    }

                    routesLocker = false;
                }
                catch (Exception ex)
                {
                    routesLocker = false;
                    Console.WriteLine(ex.Message);
                }

                return result;
            }
        }

        public async Task<string> ChooseRouteRequest(int routeId)
        {
            if (!await IsNetworkAvailable() || routeId == LoadedRouteId || chooseRoutesLocker || !IsLoggedIn)
            {
                return null;
            }

            chooseRoutesLocker = true;

            if (routeId < 1)
            {
                return "Wrong route ID";
            }

            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth", _credentialsToken);
                httpClient.DefaultRequestHeaders.Add("X-Token", _token);

                var result = await httpClient.PostAsync(Api + "/route/set/" + routeId, null);
                var response = await result.Content.ReadAsStringAsync();

                var decodedResponse = JObject.Parse(response);
                var status = decodedResponse?.SelectToken("status").Value<string>();

                chooseRoutesLocker = false;
                if (status != "OK")
                {
                    var message = decodedResponse?.SelectToken("message").Value<string>();
                    return message;
                }

                LocationService.GetInstance().Start();

                return "OK";
            }
        }

        public async Task<string> GetStopsRequest(int routeId)
        {
            if (!await IsNetworkAvailable() || routeId == LoadedRouteId || stopsLocker)
            {
                return null;
            }

            stopsLocker = true;
            if (routeId < 1)
            {
                return "Wrong route ID";
            }

            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth", _credentialsToken);
                httpClient.DefaultRequestHeaders.Add("X-Token", _token);

                var result = await httpClient.GetAsync(Api + "/stops/" + routeId);
                var response = await result.Content.ReadAsStringAsync();

                var decodedResponse = JObject.Parse(response);
                var status = decodedResponse?.SelectToken("status").Value<string>();


                if (status != "OK")
                {
                    stopsLocker = false;
                    var message = decodedResponse?.SelectToken("message").Value<string>();
                    return message;
                }

                CurrentDriver.StopList.Clear();
                try
                {
                    var stopsArr = decodedResponse.SelectToken("stops") as JArray;
                    if (stopsArr != null)
                    {
                        foreach (var jTokenStop in stopsArr)
                        {
                            var jToken = (JObject) jTokenStop;
                            var stopModel = new StopModel
                            {
                                Id = Convert.ToInt32(jToken.SelectToken("id")?.Value<int>()),
                                Title = jToken.SelectToken("title")?.Value<string>(),
                                Time = jToken.SelectToken("time")?.Value<string>(),
                                Lat = Convert.ToDecimal(jToken.SelectToken("lat")?.Value<Decimal>()),
                                Lng = Convert.ToDecimal(jToken.SelectToken("lng")?.Value<Decimal>()),
                                Play = Convert.ToBoolean(jToken.SelectToken("play")?.Value<bool>()),
                                SoundUrl = jToken.SelectToken("sound_url")?.Value<string>(),
                            };
                            CurrentDriver.StopList.Add(stopModel);
                        }
                    }

                    stopsLocker = false;
                }
                catch (Exception ex)
                {
                    stopsLocker = false;
                    Console.WriteLine(ex.Message);
                    return ex.Message;
                }

                return "OK";
            }
        }

        public async void LoadSoundsRequest(int routeId)
        {
            if (!await IsNetworkAvailable() || !IsLoggedIn || loadSoundsLocker)
            {
                return;
            }

            loadSoundsLocker = true;

            if (routeId != LoadedRouteId)
            {
                await GetStopsRequest(routeId);
            }

            var resourceService = ResourceService.Current;
            foreach (var stopModel in CurrentDriver.StopList)
            {
                var url = stopModel.SoundUrl;
                if (!string.IsNullOrEmpty(url))
                {
                    resourceService.Queue.Add(url);
                }
            }

            resourceService.DownloadStart();
        }

        public async Task<HttpResponseMessage> LogoutRequest()
        {
            if (!await IsNetworkAvailable() || !IsLoggedIn || logoutLocker)
            {
                return null;
            }

            logoutLocker = true;
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth", _credentialsToken);
                httpClient.DefaultRequestHeaders.Add("X-Token", _token);
                var result = await httpClient.PostAsync(Api + "/logout", null);
                var response = await result.Content.ReadAsStringAsync();

                var decodedResponse = JsonConvert.DeserializeObject(response) as JObject;
                var status = decodedResponse?.SelectToken("status").Value<string>();
                var loggedOut = Convert.ToBoolean(decodedResponse?.SelectToken("loggedOut").Value<bool>());

                if (status == "OK" && loggedOut)
                {
                    OnLoginStateChange(false, decodedResponse);
                    _credentialsToken = "";
                    _token = "";
                    _loggedIn = false;

                    LocationService.GetInstance().Stop();
                    CurrentDriver.Clear();
                }
                else
                {
                    OnLoginStateChange(true, decodedResponse);
                }

                logoutLocker = false;

                return result;
            }
        }
    }
}